/* eslint-disable no-undef */

// add 1 second latency
module.exports = (req, res, next) => {
	setTimeout(() => {
		next();
	}, 1000);
};

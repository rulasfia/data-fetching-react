import { createBrowserRouter } from "react-router-dom";
import { Root } from "./pages/root";
import { ReactPage } from "./pages/react";
import { TanstackWithRouterPage } from "./pages/tanstack-router-integration";
import { TanstackPage } from "./pages/tanstack";
import { RerouterPage } from "./pages/rerouter";
import { AddTanstackPage } from "./pages/add-tanstack";
import { AddReactPage } from "./pages/add-react";
import { TanstackWithFilterPage } from "./pages/tanstack-with-filter";

export const router = createBrowserRouter([
	{
		path: "/login",
		element: <div>Login</div>,
	},
	{
		path: "/",
		element: <Root />,
		children: [
			{
				path: "/add-with-react",
				element: <AddReactPage />,
			},
			{
				path: "/add-with-tanstack",
				element: <AddTanstackPage />,
			},
			{
				path: "/react",
				element: <ReactPage />,
			},
			{
				path: "/tanstack",
				element: <TanstackPage />,
			},
			{
				path: "/tanstack-with-filter",
				element: <TanstackWithFilterPage />,
			},
			{
				path: "/rerouter",
				// loader: async () => {},
				element: <RerouterPage />,
			},
			{
				path: "/tanstack-router-integration",
				element: <TanstackWithRouterPage />,
			},
		],
	},
]);

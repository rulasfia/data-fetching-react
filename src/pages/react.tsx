import { useEffect, useState } from "react";
import { API_URL } from "../constants";
import { Product, RequestStatusType } from "../types";
import { Products } from "../components/products";
import { Toolbar } from "../components/toolbar";

// create reusable custom hooks for fetching data from API
function useFetch<IData>(url: string) {
	const [data, setData] = useState<undefined | IData>();

	const [status, setStatus] = useState<RequestStatusType>("pending");
	const [error, setError] = useState("");

	useEffect(() => {
		setStatus("loading");
		fetch(url)
			.then((res) => res.json())
			.then((data) => {
				setData(data);
				setStatus("success");
			})
			.catch((err) => {
				setError(err);
				setStatus("error");
			});
	}, [url]);

	return { data, status, error };
}

export function ReactPage() {
	// use custom hook to fetch data from API with returned data type as generic
	const { data, status, error } = useFetch<Product[]>(`${API_URL}/products`);

	return (
		<div>
			<Toolbar />
			{status === "loading" && <span>loading...</span>}
			{status === "error" && <span>{error}</span>}
			{status === "success" && <Products data={data ?? []} />}
		</div>
	);
}

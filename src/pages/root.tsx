import { Link, Outlet } from "react-router-dom";
import styles from "./root.module.css";

const links = [
	{
		path: "/react",
		label: "React",
	},
	{
		path: "/tanstack",
		label: "Tanstack Query",
	},
	{
		path: "/tanstack-with-filter",
		label: "Tanstack With Filter",
	},
	{
		path: "/rerouter",
		label: "React Router",
	},
	{
		path: "/tanstack-router-integration",
		label: "Tanstack Query with Router Integration",
	},
	{
		path: "/with-kubb",
		label: "+ Kubb",
	},
];

export function Root() {
	return (
		<div>
			<h1 className={styles.title}>Root Layout</h1>
			<Header />
			<main className={styles["main-container"]}>
				<Outlet />
			</main>
		</div>
	);
}

function Header() {
	return (
		<nav className={styles.navigation}>
			<span>{">>>"}</span>
			{links.map((item) => (
				<Link key={item.path} to={item.path}>
					{item.label}
				</Link>
			))}
			<span>{"<<<"}</span>
		</nav>
	);
}

import { useState } from "react";
import styles from "./add-react.module.css";
import { RequestStatusType } from "../types";
import { useNavigate } from "react-router-dom";
import { API_URL } from "../constants";

export function AddReactPage() {
	const navigate = useNavigate();
	const [status, setStatus] = useState<RequestStatusType>("pending");

	const onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
		e.preventDefault();
		setStatus("loading");
		const form = e.currentTarget;
		const formData = new FormData(form);
		const data = Object.fromEntries(formData);

		console.log(data);

		fetch(`${API_URL}/products`, {
			method: "POST",
			body: JSON.stringify({ ...data, id: crypto.randomUUID() }),
			headers: {
				"Content-Type": "application/json",
			},
		})
			.then(() => setStatus("success"))
			.then(() => navigate("/react"))
			.catch(() => setStatus("error"));
	};

	return (
		<div>
			<h2>Add Product (react)</h2>
			<form onSubmit={onSubmit} className={styles.form}>
				<label className={styles.textfield}>
					<span>Name</span>
					<input type="text" name="name" id="name" />
				</label>
				<label className={styles.textfield}>
					<span>Description</span>
					<input type="text" name="description" id="description" />
				</label>
				<label className={styles.textfield}>
					<span>Price</span>
					<input type="number" name="price" id="price" />
				</label>

				<button
					className={styles["submit-button"]}
					type="submit"
					disabled={status === "loading"}
				>
					{status === "loading" ? "Loading..." : "Submit"}
				</button>
			</form>
		</div>
	);
}

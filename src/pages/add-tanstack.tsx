import { z } from "zod";
import styles from "./add-react.module.css";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { useNavigate } from "react-router-dom";
import { API_URL } from "../constants";
import { useMutation, useQueryClient } from "@tanstack/react-query";

const addProductSchema = z.object({
	name: z.string().min(1),
	description: z.string().min(1),
	price: z.string().min(1).transform(Number),
});

export function AddTanstackPage() {
	const qc = useQueryClient();
	const navigate = useNavigate();

	const { register, handleSubmit } = useForm<z.infer<typeof addProductSchema>>({
		resolver: zodResolver(addProductSchema),
	});

	const { mutate, status } = useMutation({
		mutationFn: async (data: z.infer<typeof addProductSchema>) => {
			console.log(data);
			return fetch(`${API_URL}/products`, {
				method: "POST",
				body: JSON.stringify({ ...data, id: crypto.randomUUID() }),
				headers: {
					"Content-Type": "application/json",
				},
			});
		},
		onSuccess: () => {
			qc.invalidateQueries({ queryKey: ["products"] });
			navigate("/tanstack");
		},
	});

	return (
		<div>
			<h2>Add Product (rhf + tanstack)</h2>
			<form onSubmit={handleSubmit((v) => mutate(v))} className={styles.form}>
				<label className={styles.textfield}>
					<span>Name</span>
					<input type="text" {...register("name")} />
				</label>
				<label className={styles.textfield}>
					<span>Description</span>
					<input type="text" {...register("description")} />
				</label>
				<label className={styles.textfield}>
					<span>Price</span>
					<input type="number" {...register("price")} />
				</label>

				<button
					className={styles["submit-button"]}
					type="submit"
					disabled={status === "pending"}
				>
					{status === "pending" ? "Loading..." : "Submit"}
				</button>
			</form>
		</div>
	);
}

import { UndefinedInitialDataOptions, useQuery } from "@tanstack/react-query";
import { Toolbar } from "../components/toolbar";
import { Products } from "../components/products";
import { API_URL } from "../constants";
import { Product } from "../types";
import { useState } from "react";

// create reusable query arguments for useQuery call
const productsQuery = (
	props: UndefinedInitialDataOptions<Product[]>
): UndefinedInitialDataOptions<Product[]> => {
	return {
		queryKey: ["products"],
		queryFn: () => fetch(`${API_URL}/products`).then((res) => res.json()),
		staleTime: 2 * 60 * 1000, // 2 min
		...props,
	};
};

export function TanstackWithFilterPage() {
	const [keyword, setKeyword] = useState("");

	const { status, error, data } = useQuery(
		productsQuery({
			queryKey: ["products", keyword],
			queryFn: () =>
				fetch(`${API_URL}/products?q=${keyword}`).then((res) => res.json()),
		})
	);

	return (
		<div>
			<Toolbar keyword={keyword} setKeyword={setKeyword} />
			{status === "pending" && <span>loading...</span>}
			{status === "error" && <span>{error.message}</span>}
			{status === "success" && <Products data={data ?? []} />}
		</div>
	);
}

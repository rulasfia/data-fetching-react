import { UndefinedInitialDataOptions, useQuery } from "@tanstack/react-query";
import { Toolbar } from "../components/toolbar";
import { Products } from "../components/products";
import { API_URL } from "../constants";
import { Product } from "../types";

// create reusable query arguments for useQuery call
const productsQuery: UndefinedInitialDataOptions<Product[]> = {
	queryKey: ["products"],
	queryFn: () => fetch(`${API_URL}/products`).then((res) => res.json()),
	staleTime: 2 * 60 * 1000, // 2 min
};

export function TanstackPage() {
	const { status, error, data } = useQuery(productsQuery);

	return (
		<div>
			<Toolbar />
			{status === "pending" && <span>loading...</span>}
			{status === "error" && <span>{error.message}</span>}
			{status === "success" && <Products data={data ?? []} />}
		</div>
	);
}

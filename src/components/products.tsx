import { Link } from "react-router-dom";
import { Product } from "../types";
import styles from "./products.module.css";

type ComponentProps = {
	data: Product[];
};

export function Products({ data }: ComponentProps) {
	return (
		<div className={styles["products-container"]}>
			{data.map((item) => (
				<div key={item.id} className={styles["product-card"]}>
					<span className={styles.pricing}>${item.price}</span>
					<Link to={`/product/${item.id}`} className={styles.title}>
						{item.name}
					</Link>
					<p className={styles.desc}>{item.description}</p>
				</div>
			))}
		</div>
	);
}

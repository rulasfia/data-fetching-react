import { Link } from "react-router-dom";
import styles from "./toolbar.module.css";

export function Toolbar({
	keyword,
	setKeyword,
}: {
	keyword?: string;
	setKeyword?: (v: string) => void;
}) {
	return (
		<div className={styles["toolbar-container"]}>
			<div className={styles["search-container"]}>
				<input
					className={styles["search-input"]}
					type="text"
					placeholder="Search"
					value={keyword ?? ""}
					onChange={(e) => (setKeyword ? setKeyword(e.target.value) : null)}
				/>
			</div>
			<Link to="/add-with-react" className={styles["addproduct-button"]}>
				Add Product (react)
			</Link>
			<Link to="/add-with-tanstack" className={styles["addproduct-button"]}>
				Add Product (rhf + tanstack)
			</Link>
		</div>
	);
}

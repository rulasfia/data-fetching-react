export type Product = {
	id: string;
	name: string;
	price: number;
	description: string;
};

export type RequestStatusType = "pending" | "loading" | "error" | "success";
